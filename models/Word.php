<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "word".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $comment
 * @property integer $dict_id
 *
 * @property Dict $dict
 */
class Word extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment'], 'string'],
            [['dict_id'], 'integer'],
            [['name_ru', 'name_en'], 'string', 'max' => 255],
            [['dict_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dict::className(), 'targetAttribute' => ['dict_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'comment' => 'Comment',
            'dict_id' => 'Dict ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDict()
    {
        return $this->hasOne(Dict::className(), ['id' => 'dict_id']);
    }
}
