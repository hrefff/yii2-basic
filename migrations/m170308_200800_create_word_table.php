<?php

use yii\db\Migration;

/**
 * Handles the creation of table `word`.
 */
class m170308_200800_create_word_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            $tableOptions = 'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci';
//        }
        
        $this->createTable('{{%word}}', [
            'id' => $this->primaryKey(),
            'name_ru' => $this->string(),
            'name_en' => $this->string(),
            'comment' => $this->text(),
            'dict_id' => $this->integer(),
        ]);

        $this->createIndex('idx_word_dist', '{{%word}}', 'dict_id');
        $this->addForeignKey('fk_post_author', '{{%word}}', 'dict_id', '{{%dict}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%word}}');
    }
}
