<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dict`.
 */
class m170308_200744_create_dict_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%dict}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%dict}}');
    }
}
